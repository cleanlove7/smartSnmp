package com.seven.smartsnmp.handler;

import com.seven.smartsnmp.base.MyException;
import org.snmp4j.Snmp;
import org.snmp4j.Target;

/**
 * @author Admin
 */
public class AddHandler {
    /**
     * 添加表格
     * @param obj
     * @param snmp
     * @param target
     * @throws MyException
     */
    public  void addTable(Object obj, Snmp snmp, Target target) throws MyException {
        HandlerFactory.getUpdateHandler().update(obj, snmp, target);
    }

}
