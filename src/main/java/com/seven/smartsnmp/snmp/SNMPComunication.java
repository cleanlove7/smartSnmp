package com.seven.smartsnmp.snmp;

import com.seven.smartsnmp.base.DefaultConstant;
import com.seven.smartsnmp.base.SnmpAndTransport;
import org.apache.log4j.Logger;
import org.snmp4j.*;
import org.snmp4j.mp.MPv1;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.*;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OctetString;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import java.io.IOException;

public class SNMPComunication {

    private static Logger log = Logger.getLogger(SNMPComunication.class);

    public static final String DEFAULT_PROTOCOL = "udp";


    public static Snmp createSnmp(SNMPTarget target) throws IOException {
        Snmp snmp = null;
        //1、初始化多线程消息转发类
        MessageDispatcher messageDispatcher = new MessageDispatcherImpl();
        //其中要增加三种处理模型。如果snmp初始化使用的是Snmp(TransportMapping<? extends Address> transportMapping) ,就不需要增加
        messageDispatcher.addMessageProcessingModel(new MPv1());
        messageDispatcher.addMessageProcessingModel(new MPv2c());
        //当要支持snmpV3版本时，需要配置user
        if (target.snmpVersion == SnmpConstants.version3) {
            USM usm = new USM(SecurityProtocols.getInstance(), new OctetString(MPv3.createLocalEngineID()), 0);
            UsmUser user = new UsmUser(
                    new OctetString("SNMPV3"),
                    AuthSHA.ID,
                    new OctetString(target.v3AuthPasswd),
                    PrivAES128.ID,
                    new OctetString(target.v3PrivacyPasswd));
            usm.addUser(user.getSecurityName(), user);
            messageDispatcher.addMessageProcessingModel(new MPv3(usm));
        }
        //2、创建transportMapping
        TransportMapping transportMapping = new DefaultUdpTransportMapping();
        //3、正式创建snmp
        snmp = new Snmp(messageDispatcher, transportMapping);
        //开启监听
        transportMapping.listen();
        return snmp;
    }

    public static SnmpAndTransport createSnmpAndTransport(SNMPTarget target) throws IOException {
        Snmp snmp = null;
        //1、初始化多线程消息转发类
        MessageDispatcher messageDispatcher = new MessageDispatcherImpl();
        //其中要增加三种处理模型。如果snmp初始化使用的是Snmp(TransportMapping<? extends Address> transportMapping) ,就不需要增加
        messageDispatcher.addMessageProcessingModel(new MPv1());
        messageDispatcher.addMessageProcessingModel(new MPv2c());
        //当要支持snmpV3版本时，需要配置user
        if (target.snmpVersion == SnmpConstants.version3) {
            USM usm = new USM(SecurityProtocols.getInstance(), new OctetString(MPv3.createLocalEngineID()), 0);
            UsmUser user = new UsmUser(
                    new OctetString("SNMPV3"),
                    AuthSHA.ID,
                    new OctetString(target.v3AuthPasswd),
                    PrivAES128.ID,
                    new OctetString(target.v3PrivacyPasswd));
            usm.addUser(user.getSecurityName(), user);
            messageDispatcher.addMessageProcessingModel(new MPv3(usm));
        }
        //2、创建transportMapping
        TransportMapping transportMapping = new DefaultUdpTransportMapping();
        //3、正式创建snmp
        snmp = new Snmp(messageDispatcher, transportMapping);
        //开启监听
        transportMapping.listen();
        return new SnmpAndTransport(snmp, transportMapping);
    }


    /**
     * 关闭流的通用方法
     *
     * @param snmp
     */
    public static void closeSnmp(Snmp snmp) {
        try {
            if (snmp != null) {
                snmp.close();
            }
        } catch (Exception e) {
            log.error("关闭snmp失败", e);
        }
    }


    public static PDU createPdu(int version) {
        PDU pdu = null;
        if (version == SnmpConstants.version3) {
            pdu = new ScopedPDU();
        } else {
            pdu = new PDUv1();
        }
        return pdu;
    }

    //2.创建目标地址对象
    public static Target getTarget(SNMPTarget snmpTarget, int targetType) {
        if (!(snmpTarget.snmpVersion == SnmpConstants.version3 || snmpTarget.snmpVersion == SnmpConstants.version2c || snmpTarget.snmpVersion == SnmpConstants.version1)) {
            log.error("参数version异常");
            return null;
        }
        Address address = GenericAddress.parse(DEFAULT_PROTOCOL + ":" + snmpTarget.nodeIP + "/" + snmpTarget.port);
        if (snmpTarget.snmpVersion == SnmpConstants.version3) {
            UserTarget target = new UserTarget();
            //IP+Port
            target.setAddress(address);
            //Version
            target.setVersion(snmpTarget.snmpVersion);
            //timeout
            target.setTimeout(snmpTarget.targetSnmpTimeout);
            //Retries
            target.setRetries(snmpTarget.targetSnmpRetry);
            target.setSecurityLevel(snmpTarget.v3SecurityLevel);
            target.setSecurityName(new OctetString(snmpTarget.v3User));
            target.setSecurityModel(snmpTarget.v3PrivacyProtocol);
            return target;
        } else {
            CommunityTarget target = new CommunityTarget();
            //IP+Port
            target.setAddress(address);
            //Version
            target.setVersion(snmpTarget.snmpVersion);
            //Community
            //targetType  1:SET  0：GET
            if (targetType == DefaultConstant.SNMP_COMMUNITY_SET) {
                target.setCommunity(new OctetString(snmpTarget.writeCommunity));
            } else {
                target.setCommunity(new OctetString(snmpTarget.readCommunity));
            }
            target.setTimeout(snmpTarget.targetSnmpTimeout);
            //Retries
            target.setRetries(snmpTarget.targetSnmpRetry);
            return target;
        }
    }


}
