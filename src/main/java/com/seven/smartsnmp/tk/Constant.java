package com.seven.smartsnmp.tk;

/**
 * Created by heer on 2018/7/30.
 */
public class Constant {

    public static final String PACKET_NAME = "com.seven.smartsnmp.mib";//包名
    public static final String SAVE_PATH = "D:\\mibcode\\";//类文件路径
    public static final String REGEX = ",";//分隔符
    public static final String BASIC_ENTITY_STRING = "OMMappingInfo";//基类包加名
    public static final String JAVA_FILE_SUFFIX = ".java";//java文件后缀
    public static final String CODE_NOTES = "//This code is generated by seven!";//生成的类文件所添加的注释
}
