package com.seven.smartsnmp.tk;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import com.seven.smartsnmp.snmp.SNMPFactory;
import net.percederberg.mibble.MibType;
import net.percederberg.mibble.MibValueSymbol;
import net.percederberg.mibble.snmp.SnmpObjectType;

import java.io.FileOutputStream;
import java.io.PrintStream;

public class GenJavaCodeTool {
    private static String srcDirectory = "";
    private static String packageName = "";

    public GenJavaCodeTool() {
    }

    public static void setJavaCodeDirectory(String var0, String var1) {
        srcDirectory = var0;
        packageName = var1;
    }

    private static String getType(SnmpObjectType var0) {
        MibType var1 = var0.getSyntax();
        if (var1.hasTag(0, 2)) {
            return "int";
        } else if (var1.hasTag(1, 1)) {
            return "long";
        } else if (var1.hasTag(1, 2)) {
            return "long";
        } else if (var1.hasTag(0, 4)) {
            return "String";
        } else if (var1.hasTag(0, 6)) {
            return "String";
        } else if (var1.hasTag(1, 0)) {
            return "String";
        } else if (var1.hasTag(1, 3)) {
            return "long";
        } else if (var1.hasTag(1, 4)) {
            return "String";
        } else if (var1.hasTag(1, 6)) {
            return "long";
        } else {
            System.err.print("未知的SNMP MIB数据类型: " + var1 + "，  将用String类型来映射（仅用于SNMP查询操作）");
            return "    String";
        }
    }

    public static void genJavaCode(String var0, String var1) {
        try {
            MibValueSymbol var2 = SNMPFactory.getInstance().getMibSymbolByOid(var1);
            PrintStream var3 = new PrintStream(new FileOutputStream(srcDirectory + var0 + ".java"));
            System.setOut(var3);
            System.out.println("package " + packageName + " ;");
            System.out.println("//GenJavaCodeTool自动生成的OM对象代码！");
            System.out.println("public class " + var0 + " extends  com.seven.smartsnmp.base.OMMappingInfo");
            System.out.println("{");
            MibValueSymbol[] var4 = var2.getChildren();

            for(int var5 = 0; var5 < var4.length; ++var5) {
                MibValueSymbol var6 = var4[var5];
                if (!var6.isTable() && !var6.isTableRow()) {
                    String var7 = var6.getName();
                    SnmpObjectType var8 = (SnmpObjectType)var6.getType();
                    String var9 = getType(var8);
                    System.out.println("    private " + var9 + " " + var7 + " ;");
                    System.out.println();
                }
            }

            System.out.println("    public String toString()");
            System.out.println("    {");
            StringBuffer var13 = new StringBuffer("        return ");

            int var14;
            MibValueSymbol var15;
            String var16;
            for(var14 = 0; var14 < var4.length; ++var14) {
                var15 = var4[var14];
                if (!var15.isTable() && !var15.isTableRow()) {
                    var16 = var15.getName();
                    var13.append("\"" + var16 + "=\"+" + var16 + "+\"|\"+");
                }
            }

            var13.deleteCharAt(var13.length() - 1);
            var13.append(" ;");
            System.out.println(var13);
            System.out.println("    }");
            System.out.println();
            System.out.println("    public String getMappingOID()");
            System.out.println("    {");
            System.out.println("        return \"" + var1 + "\";");
            System.out.println("    }");

            for(var14 = 0; var14 < var4.length; ++var14) {
                var15 = var4[var14];
                if (!var15.isTable() && !var15.isTableRow()) {
                    var16 = var15.getName();
                    SnmpObjectType var17 = (SnmpObjectType)var15.getType();
                    String var10 = getType(var17);
                    String var11 = var16.replaceFirst(var16.substring(0, 1), var16.substring(0, 1).toUpperCase());
                    System.out.println("    public " + var10 + " get" + var11 + "()");
                    System.out.println("    {");
                    System.out.println("        return " + var16 + " ;");
                    System.out.println("    }");
                    System.out.println("    public void set" + var11 + "(" + var10 + " value)");
                    System.out.println("    {");
                    System.out.println("        " + var16 + "=value ;");
                    System.out.println("    }");
                }
            }

            System.out.println("}");
            var3.close();
        } catch (Exception var12) {
            var12.printStackTrace();
        }

        System.setOut((PrintStream)null);
    }


}
