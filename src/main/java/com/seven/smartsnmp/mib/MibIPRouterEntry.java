package com.seven.smartsnmp.mib;


import com.seven.smartsnmp.base.OMMappingInfo;

/**
 * @author seven
 * @create 2019-06-06 3:34
 **/
public class MibIPRouterEntry extends OMMappingInfo {
    private String ipRouteDest;
    private int ipRouteIfIndex;
    private int ipRouteMetric1;
    private int ipRouteMetric2;
    private int ipRouteMetric3;
    private int ipRouteMetric4;
    private String ipRouteNextHop;
    private int ipRouteType;
    private int ipRouteProto;
    private int ipRouteAge;
    private String ipRouteMask;
    private int ipRouteMetric5;
    private String ipRouteInfo;

    public MibIPRouterEntry() {
    }
    @Override
    public String toString() {
        return "ipRouteDest=" + this.ipRouteDest + "|" + "ipRouteIfIndex=" + this.ipRouteIfIndex + "|" + "ipRouteMetric1=" + this.ipRouteMetric1 + "|" + "ipRouteMetric2=" + this.ipRouteMetric2 + "|" + "ipRouteMetric3=" + this.ipRouteMetric3 + "|" + "ipRouteMetric4=" + this.ipRouteMetric4 + "|" + "ipRouteNextHop=" + this.ipRouteNextHop + "|" + "ipRouteType=" + this.ipRouteType + "|" + "ipRouteProto=" + this.ipRouteProto + "|" + "ipRouteAge=" + this.ipRouteAge + "|" + "ipRouteMask=" + this.ipRouteMask + "|" + "ipRouteMetric5=" + this.ipRouteMetric5 + "|" + "ipRouteInfo=" + this.ipRouteInfo + "|";
    }
    @Override
    public String getMappingOID() {
        return "1.3.6.1.2.1.4.21.1";
    }

    public String getIpRouteDest() {
        return this.ipRouteDest;
    }

    public void setIpRouteDest(String ipRouteDest) {
        this.ipRouteDest = ipRouteDest;
    }

    public int getIpRouteIfIndex() {
        return this.ipRouteIfIndex;
    }

    public void setIpRouteIfIndex(int var1) {
        this.ipRouteIfIndex = var1;
    }

    public int getIpRouteMetric1() {
        return this.ipRouteMetric1;
    }

    public void setIpRouteMetric1(int var1) {
        this.ipRouteMetric1 = var1;
    }

    public int getIpRouteMetric2() {
        return this.ipRouteMetric2;
    }

    public void setIpRouteMetric2(int var1) {
        this.ipRouteMetric2 = var1;
    }

    public int getIpRouteMetric3() {
        return this.ipRouteMetric3;
    }

    public void setIpRouteMetric3(int var1) {
        this.ipRouteMetric3 = var1;
    }

    public int getIpRouteMetric4() {
        return this.ipRouteMetric4;
    }

    public void setIpRouteMetric4(int var1) {
        this.ipRouteMetric4 = var1;
    }

    public String getIpRouteNextHop() {
        return this.ipRouteNextHop;
    }

    public void setIpRouteNextHop(String var1) {
        this.ipRouteNextHop = var1;
    }

    public int getIpRouteType() {
        return this.ipRouteType;
    }

    public void setIpRouteType(int var1) {
        this.ipRouteType = var1;
    }

    public int getIpRouteProto() {
        return this.ipRouteProto;
    }

    public void setIpRouteProto(int var1) {
        this.ipRouteProto = var1;
    }

    public int getIpRouteAge() {
        return this.ipRouteAge;
    }

    public void setIpRouteAge(int var1) {
        this.ipRouteAge = var1;
    }

    public String getIpRouteMask() {
        return this.ipRouteMask;
    }

    public void setIpRouteMask(String var1) {
        this.ipRouteMask = var1;
    }

    public int getIpRouteMetric5() {
        return this.ipRouteMetric5;
    }

    public void setIpRouteMetric5(int var1) {
        this.ipRouteMetric5 = var1;
    }

    public String getIpRouteInfo() {
        return this.ipRouteInfo;
    }

    public void setIpRouteInfo(String var1) {
        this.ipRouteInfo = var1;
    }
}
