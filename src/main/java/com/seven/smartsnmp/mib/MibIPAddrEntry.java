package com.seven.smartsnmp.mib;


import com.seven.smartsnmp.base.OMMappingInfo;

/**
 * @author seven
 * @create 2019-06-06 3:28
 **/
public class MibIPAddrEntry extends OMMappingInfo {
    private String ipAdEntAddr;
    private int ipAdEntIfIndex;
    private String ipAdEntNetMask;
    private String ipAdEntBcastAddr;
    private int ipAdEntReasmMaxSize;

    public MibIPAddrEntry() {
    }
    @Override
    public String toString() {
        return "ipAdEntAddr=" + this.ipAdEntAddr + "|" + "ipAdEntIfIndex=" + this.ipAdEntIfIndex + "|" + "ipAdEntNetMask=" + this.ipAdEntNetMask + "|" + "ipAdEntBcastAddr=" + this.ipAdEntBcastAddr + "|" + "ipAdEntReasmMaxSize=" + this.ipAdEntReasmMaxSize + "|";
    }
    @Override
    public String getMappingOID() {
        return "1.3.6.1.2.1.4.20.1";
    }

    public String getIpAdEntAddr() {
        return this.ipAdEntAddr;
    }

    public void setIpAdEntAddr(String var1) {
        this.ipAdEntAddr = var1;
    }

    public int getIpAdEntIfIndex() {
        return this.ipAdEntIfIndex;
    }

    public void setIpAdEntIfIndex(int var1) {
        this.ipAdEntIfIndex = var1;
    }

    public String getIpAdEntNetMask() {
        return this.ipAdEntNetMask;
    }

    public void setIpAdEntNetMask(String var1) {
        this.ipAdEntNetMask = var1;
    }

    public String getIpAdEntBcastAddr() {
        return this.ipAdEntBcastAddr;
    }

    public void setIpAdEntBcastAddr(String var1) {
        this.ipAdEntBcastAddr = var1;
    }

    public int getIpAdEntReasmMaxSize() {
        return this.ipAdEntReasmMaxSize;
    }

    public void setIpAdEntReasmMaxSize(int var1) {
        this.ipAdEntReasmMaxSize = var1;
    }

}
