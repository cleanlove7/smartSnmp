package com.seven.smartsnmp.mib;


import com.seven.smartsnmp.base.OMMappingInfo;

/**
 * @author seven
 * @create 2019-06-06 3:35
 **/
public class MibMacIP extends OMMappingInfo {
    private int ipNetToMediaIfIndex;
    private String ipNetToMediaPhysAddress;
    private String ipNetToMediaNetAddress;
    private int ipNetToMediaType;

    public MibMacIP() {
    }
    @Override
    public String toString() {
        return "MAC地址:" + this.ipNetToMediaPhysAddress + ",IP地址:" + this.ipNetToMediaNetAddress;
    }
    @Override
    public String getMappingOID() {
        return "1.3.6.1.2.1.4.22.1";
    }

    public int getIpNetToMediaIfIndex() {
        return this.ipNetToMediaIfIndex;
    }

    public void setIpNetToMediaIfIndex(int var1) {
        this.ipNetToMediaIfIndex = var1;
    }

    public String getIpNetToMediaPhysAddress() {
        return this.ipNetToMediaPhysAddress;
    }

    public void setIpNetToMediaPhysAddress(String var1) {
        this.ipNetToMediaPhysAddress = var1;
    }

    public String getIpNetToMediaNetAddress() {
        return this.ipNetToMediaNetAddress;
    }

    public void setIpNetToMediaNetAddress(String var1) {
        this.ipNetToMediaNetAddress = var1;
    }

    public int getIpNetToMediaType() {
        return this.ipNetToMediaType;
    }

    public void setIpNetToMediaType(int var1) {
        this.ipNetToMediaType = var1;
    }
}
