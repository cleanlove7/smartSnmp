package com.seven.smartsnmp.mib;

import com.seven.smartsnmp.base.OMMappingInfo;

//GenJavaCodeTool自动生成的OM对象代码！
public class MibHrSystem extends OMMappingInfo {
    private long hrSystemUptime;

    private String hrSystemDate;

    private int hrSystemInitialLoadDevice;

    private String hrSystemInitialLoadParameters;

    private long hrSystemNumUsers;

    private long hrSystemProcesses;

    private int hrSystemMaxProcesses;

    public String toString() {
        return "hrSystemUptime=" + hrSystemUptime + "|" + "hrSystemDate=" + hrSystemDate + "|" + "hrSystemInitialLoadDevice=" + hrSystemInitialLoadDevice + "|" + "hrSystemInitialLoadParameters=" + hrSystemInitialLoadParameters + "|" + "hrSystemNumUsers=" + hrSystemNumUsers + "|" + "hrSystemProcesses=" + hrSystemProcesses + "|" + "hrSystemMaxProcesses=" + hrSystemMaxProcesses + "|";
    }

    public String getMappingOID() {
        return ".1.3.6.1.2.1.25.1";
    }

    public long getHrSystemUptime() {
        return hrSystemUptime;
    }

    public void setHrSystemUptime(long value) {
        hrSystemUptime = value;
    }

    public String getHrSystemDate() {
        return hrSystemDate;
    }

    public void setHrSystemDate(String value) {
        hrSystemDate = value;
    }

    public int getHrSystemInitialLoadDevice() {
        return hrSystemInitialLoadDevice;
    }

    public void setHrSystemInitialLoadDevice(int value) {
        hrSystemInitialLoadDevice = value;
    }

    public String getHrSystemInitialLoadParameters() {
        return hrSystemInitialLoadParameters;
    }

    public void setHrSystemInitialLoadParameters(String value) {
        hrSystemInitialLoadParameters = value;
    }

    public long getHrSystemNumUsers() {
        return hrSystemNumUsers;
    }

    public void setHrSystemNumUsers(long value) {
        hrSystemNumUsers = value;
    }

    public long getHrSystemProcesses() {
        return hrSystemProcesses;
    }

    public void setHrSystemProcesses(long value) {
        hrSystemProcesses = value;
    }

    public int getHrSystemMaxProcesses() {
        return hrSystemMaxProcesses;
    }

    public void setHrSystemMaxProcesses(int value) {
        hrSystemMaxProcesses = value;
    }
}
