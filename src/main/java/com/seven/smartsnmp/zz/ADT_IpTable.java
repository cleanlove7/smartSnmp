package com.seven.smartsnmp.zz;

public class ADT_IpTable {
	public String Key; //
	public String KeyPort;
	public String KeyIp;
	
	public String DestIp;
	public String DestMAC;
	public String ConnectType;//
	
	public ADT_IpTable(String Key_){
		Key = Key_;
		
		if(null != Key){
			int Pos = Key.indexOf(".");
			if(-1 != Pos){
				KeyPort = Key.substring(0, Pos);
				KeyIp = Key.substring(Pos+1);
			}
		}
	}
}
