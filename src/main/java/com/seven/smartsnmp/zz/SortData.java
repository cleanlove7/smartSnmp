package com.seven.smartsnmp.zz;

import org.snmp4j.mp.SnmpConstants;

import java.io.IOException;
import java.util.Vector;

public class SortData {
	private int ver2c = SnmpConstants.version2c; //SnmpConstants.version2c;
	private String ip = "udp:127.0.0.1/161"; //183.60.237.167 127.0.0.1
	private String comu = "public";//community public xuleigood
	private SnmpGenerator m_snmp = new SnmpGenerator(ver2c);
	
	private Vector<ADT_IpTable> m_vecIp;
	private Vector<ADT_dot1dTpFdb> m_vecDot;
	private Vector<ADT_If> m_vecIf;
	private Vector<ADT_ConRelation> m_vecConn;
	
	public SortData(String ip_, String comu_){
		ip = ip_;
		comu = comu_;
	}
	public Vector<ADT_ConRelation> GetConn(){
		return m_vecConn;
	}
	public void Sort(){
		GetIPtable getIp = new GetIPtable();
		GetIf getIf = new GetIf();
		GetDot getDot = new GetDot();
		try {
			m_vecIp = getIp.GetInfo(m_snmp.instance, ip, comu);
			m_vecIf = getIf.GetInfo(m_snmp.instance, ip, comu);
			m_vecDot = getDot.GetInfo(m_snmp.instance, ip, comu);			
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			m_snmp.CloseSnmp();
		}
		PickOut();
	}
	//�ҵ�
	private void PickOut(){
		m_vecConn = new Vector<ADT_ConRelation>();
		//debug print begin
		/*for(int i=0;i<m_vecDot.size();i++){
			ADT_dot1dTpFdb dot = m_vecDot.get(i);
			System.out.println("#################   dot " + i);
			System.out.println(dot.dot1dTpFdbAddress);
			System.out.println(dot.dot1dTpFdbPort);
			System.out.println(dot.dot1dTpFdbStatus);
		}
		System.out.println("---------------------------");
		for(int i=0;i<m_vecIp.size();i++){
			ADT_IpTable it = m_vecIp.get(i);
			System.out.println("#################   ip " + i);
			System.out.println(it.DestIp);
			System.out.println(it.DestMAC);
			System.out.println(it.Key);
		}
		System.out.println("---------------------------");
		for(int i=0;i<m_vecIf.size();i++){
			ADT_If if_ = m_vecIf.get(i);
			System.out.println("#################   if_ " + i);
			System.out.println(if_.IfIndex);
			System.out.println(if_.IfMacAddr);
		}*/
		//debug print end
		for(int i=0;i<m_vecDot.size();i++){
			ADT_dot1dTpFdb dot = m_vecDot.get(i);
			ADT_IpTable it = FindCorrespondingIp(dot);
			String LocalMAC = FindCorrespondingMAC(dot);
			//System.out.println("#################   dest " + it.DestMAC);
			//System.out.println("#################   local " + LocalMAC);
			if(null != it && null != LocalMAC){
				ADT_ConRelation nc = new ADT_ConRelation(dot.dot1dTpFdbPort,LocalMAC,
						it.DestIp, it.DestMAC,it.ConnectType);
				m_vecConn.add(nc);
				//System.out.println("FROM(port:" + dot.dot1dTpFdbPort+ ", mac:" +LocalMAC+ ")===>TO(ip:(" +
				//		it.DestIp+ "),mac:(" +it.DestMAC+ "). Type-->" +it.ConnectType);
			}
		}
		
	}
	private ADT_IpTable FindCorrespondingIp(ADT_dot1dTpFdb dot){
		ADT_IpTable ret = null;
		//System.out.println("#################   iptable 11");
		for(int i=0;i<m_vecIp.size();i++){
			ADT_IpTable it = m_vecIp.get(i);
			//System.out.println(it.DestMAC  + "======" + dot.dot1dTpFdbAddress);
			if(it.DestMAC != null && it.DestMAC.length()>0 && it.DestMAC.equals(dot.dot1dTpFdbAddress)){
				//System.out.println("binggo ip!!!" + dot.dot1dTpFdbAddress);
				ret = it;
				break;
			}
		}
		//System.out.println("#################   iptable 22");
		return ret;				
	}
	private String FindCorrespondingMAC(ADT_dot1dTpFdb dot){
		String ret = null;
		//System.out.println("#################   dot 11");
		for(int i=0;i<m_vecIf.size();i++){
			ADT_If if_ = m_vecIf.get(i);
			//System.out.println(if_.IfIndex  + "~~~~~~~" + dot.dot1dTpFdbPort);
			if(if_.IfIndex.equals(dot.dot1dTpFdbPort)){
				ret = if_.IfMacAddr;
				//System.out.println("binggo if!!!" + if_.IfMacAddr);
				break;
			}
		}
		//System.out.println("#################   dot 22");
		return ret;				
	}
	
}





































