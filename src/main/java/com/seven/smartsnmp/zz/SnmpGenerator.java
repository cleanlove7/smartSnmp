package com.seven.smartsnmp.zz;

import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.smi.OctetString;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import java.io.IOException;

public class SnmpGenerator {
	public Snmp instance = null;
	private int version;
		
	public SnmpGenerator(int version) {
		try {
			this.version = version;
			TransportMapping transport = new DefaultUdpTransportMapping();
			instance = new Snmp(transport);
			if (version == SnmpConstants.version3) {
				USM usm = new USM(SecurityProtocols.getInstance(),
						new OctetString(MPv3.createLocalEngineID()), 0);
				SecurityModels.getInstance().addSecurityModel(usm);
			}
			transport.listen();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void CloseSnmp(){
		if(instance != null){
			try {
				instance.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
