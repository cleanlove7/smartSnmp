package com.seven.smartsnmp.task;

import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author seven
 * @create 2021-01-05 10:26
 **/
public class TaskQueue {

    private static final LinkedBlockingQueue<HashMap<String,Runnable>> linkedBlockingQueue = new LinkedBlockingQueue<>();

    public static LinkedBlockingQueue<HashMap<String, Runnable>> getLinkedBlockingQueue() {
        return linkedBlockingQueue;
    }
}
