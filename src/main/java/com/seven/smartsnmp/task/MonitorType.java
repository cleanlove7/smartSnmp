package com.seven.smartsnmp.task;

/**
 * @author seven
 * @create 2021-01-05 10:48
 **/
public class MonitorType<T> {

    private String value;
    // table , object ,long string
    private String type;
    private T t;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
