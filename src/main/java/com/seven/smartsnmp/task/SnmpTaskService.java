package com.seven.smartsnmp.task;

import com.seven.smartsnmp.snmp.SNMPTarget;
import org.apache.log4j.Logger;

import java.util.HashMap;

/**
 * @author seven
 * @create 2021-01-05 11:30
 **/
public class SnmpTaskService {
    /**
     * 执行任务
     */
    private static Logger log = Logger.getLogger(SnmpTaskService.class);


    /**
     * 执行任务
     *
     * @throws InterruptedException
     */
    public void doBiz() throws InterruptedException {
        HashMap map = TaskQueue.getLinkedBlockingQueue().take();
        String taskName = (map.keySet().toArray())[0].toString();
        SnmpTaskThreadPool.getInstance().execute((Runnable) map.get(taskName));
        log.info("执行" + taskName + "任务");
    }

    //创建任务
    private void createTask(MonitorType monitorType, SNMPTarget snmpTarget, String taskName) {
        SnmpMonitorTask snmpMonitorTask = new SnmpMonitorTask(monitorType, snmpTarget, taskName);
        HashMap map = new HashMap();
        map.put(taskName, snmpMonitorTask);
        TaskQueue.getLinkedBlockingQueue().add(map);

    }

    //查询任务执行情况
    public HashMap getTask() throws InterruptedException {
        HashMap map = ResQueue.getLinkedBlockingQueue().take();
        String taskName = (map.keySet().toArray())[0].toString();
        Object o = map.get(taskName);
        log.info("获取任务" + taskName + "执行结果" + o.toString());
        return map;

    }
}
