package com.seven.smartsnmp.trap;

import com.seven.smartsnmp.base.MyException;
import com.seven.smartsnmp.base.SNMPSysConfig;
import org.snmp4j.Snmp;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import java.util.LinkedList;

public class TrapReceiverServer {
    static final LinkedList<TrapMessageInfo> eventQueue = new LinkedList();
    public static void startTrapServer() throws MyException {
        try {
            System.out.println("Starting SNMP Trap Receiver......");
            UdpAddress udpAddress = new UdpAddress(SNMPSysConfig.SnmpTrapPort);
            DefaultUdpTransportMapping defaultUdpTransportMapping = new DefaultUdpTransportMapping(udpAddress);
            Snmp snmp = new Snmp(defaultUdpTransportMapping);
            snmp.addCommandResponder(new TrapReceiverImpl());
            snmp.listen();
            System.out.println("Started SNMP TRAP Receiver OK!");
        } catch (Exception var3) {
            throw new MyException("Staring SNMP TRAP Erro:" + var3.getMessage());
        }
    }
}
