package com.seven.smartsnmp.base;

import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.smi.OctetString;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import java.io.IOException;

/**
 * @author seven
 * @create 2019-06-17 11:31
 **/
public class SnmpGenerator {

    private static Snmp snmp;

    public static Snmp getInstance(int version) {
        if (snmp == null) {
            synchronized (SnmpGenerator.class) {
                if (snmp == null) {
                    try {
                        TransportMapping transport = new DefaultUdpTransportMapping();
                        snmp = new Snmp(transport);
                        if (version == SnmpConstants.version3) {
                            USM usm = new USM(SecurityProtocols.getInstance(),
                                    new OctetString(MPv3.createLocalEngineID()), 0);
                            SecurityModels.getInstance().addSecurityModel(usm);
                        }
                        transport.listen();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return snmp;
    }


}
