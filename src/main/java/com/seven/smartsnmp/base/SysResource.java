package com.seven.smartsnmp.base;

import net.percederberg.mibble.MibLoader;
import org.apache.log4j.Logger;

public class SysResource {

    private static MibLoader mibload;

    private static Logger log = Logger.getLogger(SysResource.class);

    public static MibLoader getMibload() throws MyException {
        if (mibload == null) {
            init();
        }
        return mibload;
    }


    public static void init() throws MyException {
        try {
            if (mibload == null) {
                mibload = new MibLoader();
                log.info("SNMP网关模块开始初始化！");
                log.info("请确保MIB文件放置在classpath中的mibs包中");
                mibload.load("RFC1213-MIB");
                mibload.load("HOST-RESOURCES-MIB");
                mibload.load("BRIDGE-MIB");

                log.info("加载默认的RFC1213-MIB、HOST-RESOURCES-MIB、BRIDGE-MIB成功！");
                log.info("初始化SNMP模块成功!");
            } else {
                log.info("系统已经初始化结束");
            }

        } catch (Exception e) {
            log.error("初始化SNMP模块出错:," + e.getMessage());
            log.error(e);
            e.printStackTrace();
        }
    }

}
