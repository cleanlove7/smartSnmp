package com.seven.smartsnmp.base;

import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;

public class SnmpAndTransport {
    private Snmp snmp;
    private TransportMapping transportMapping ;

    public Snmp getSnmp() {
        return snmp;
    }

    public void setSnmp(Snmp snmp) {
        this.snmp = snmp;
    }

    public TransportMapping getTransportMapping() {
        return transportMapping;
    }

    public void setTransportMapping(TransportMapping transportMapping) {
        this.transportMapping = transportMapping;
    }

    public SnmpAndTransport(Snmp snmp, TransportMapping transportMapping) {
        this.snmp = snmp;
        this.transportMapping = transportMapping;
    }
}
