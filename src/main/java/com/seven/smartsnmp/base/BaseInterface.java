package com.seven.smartsnmp.base;

import java.util.List;
import java.util.Map;

/**
 * Created by heer on 2018/8/7.
 */
public interface BaseInterface {
    Map getIndexMap();
    String getMappingOID();
    Map getValues();
    String getTableIndexOID();
    String getIndexValue();
    BaseInterface setValue(List list);
}
