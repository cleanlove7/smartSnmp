package com.seven.smartsnmp.base;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MyLog {

    public static Logger log = Logger.getLogger("global");

    public MyLog() {
    }

    public static void initLogParam(String var0) {
    }

    public static boolean err(String var0) {
        log.severe(var0);
        return true;
    }

    public static boolean warning(String var0) {
        log.warning(var0);
        return true;
    }

    public static boolean log(String var0) {
        log.info(var0);
        return true;
    }

    public static boolean goodMessage(String var0) {
        log.fine(var0);
        return true;
    }

    public static boolean debug(Exception var0) {
        log.log(Level.WARNING, var0.getMessage(), var0);
        return true;
    }

}
