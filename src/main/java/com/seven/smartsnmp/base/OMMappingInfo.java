package com.seven.smartsnmp.base;

import java.io.Serializable;

/**
 * @author Admin
 */
public abstract class OMMappingInfo implements Serializable {
    private String nodeName = "";
    private String dbKey = "";
    private String tableIndexOID = "";

    public OMMappingInfo() {
    }

    public String getDbKey() {
        return this.dbKey;
    }

    public void setDbKey(String var1) {
        this.dbKey = var1;
    }

    public String genKey() {
        return "";
    }

    public abstract String getMappingOID();

    public String getNodeName() {
        return this.nodeName;
    }

    public void setNodeName(String var1) {
        this.nodeName = var1;
    }

    public void setTableIndexOID(String var1) {
        this.tableIndexOID = var1;
    }

    public String getTableIndexOID() {
        return this.tableIndexOID;
    }
}
