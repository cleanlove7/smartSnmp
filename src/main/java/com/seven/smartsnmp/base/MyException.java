package com.seven.smartsnmp.base;

public class MyException extends Exception {
    private int type = -1;

    public MyException(String str) {
        super(str);
    }

    public MyException(Exception e) {
        super(e.getMessage());
    }

    public int getType() {
        return this.type;
    }

    private void setType(int type) {
        this.type = type;
    }
}

