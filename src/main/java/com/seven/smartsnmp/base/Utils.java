package com.seven.smartsnmp.base;

import javax.swing.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.prefs.Preferences;

public class Utils {
    static ResourceBundle res = ResourceBundle.getBundle("com.zhtelecom.common.base.Res");
    static MessageDigest md5 = null;
    static char[] hexDigits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    static {
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception var1) {
            MyLog.err("Get MD5 Function Erro:" + var1.getMessage());
        }

    }

    public Utils() {
    }

    public static boolean getosInfo(String var0) {
        boolean var1 = true;

        try {
            String var2 = new String(new char[]{'f', 'u', 't'});
            long var3 = 15552000000L;
            Preferences var5 = Preferences.userRoot().node(var0);
            SimpleDateFormat var6 = new SimpleDateFormat("ddMMyyyydd");
            String var7 = var5.get(var2, "");
            if (var7.equalsIgnoreCase("")) {
                var7 = var6.format(new Date());
                var5.put(var2, var7);
            } else {
                try {
                    var6.parse(var5.get(var2, ""));
                } catch (Exception var10) {
                    var7 = var6.format(new Date());
                    var5.put(var2, var7);
                }
            }

            long var8 = var6.parse(var7).getTime();
            if (var8 > System.currentTimeMillis()) {
                var1 = false;
            }

            if (System.currentTimeMillis() - var8 > var3) {
                var1 = false;
            }

            return var1;
        } catch (Exception var11) {
            return false;
        }
    }

    public static String classNameToNoPackageName(String var0) {
        String var1 = null;
        int var2 = var0.lastIndexOf(".");
        if (var2 > 0) {
            var1 = var0.substring(var2 + 1);
            return var1;
        } else {
            return var0;
        }
    }

    public static void setActionIcon(Action var0, String var1) {
        try {
            URL var2 = Utils.class.getClassLoader().getResource(var1);
            ImageIcon var3 = new ImageIcon(var2);
            var0.putValue("SmallIcon", var3);
        } catch (Exception var4) {
            var4.printStackTrace();
        }

    }

    public static String classToNoPackageName(Object var0) {
        return classNameToNoPackageName(var0.getClass().getName());
    }

    public static String objToClasssName(Object var0) {
        return var0.getClass().getName();
    }

    public static synchronized long getTimeIDKey() {
        long var0 = System.currentTimeMillis();
        long var2 = (new Random(872L)).nextLong();
        long var4 = UUID.randomUUID().getLeastSignificantBits();
        return var0 * var2 * var4;
    }

    public static synchronized String getMD5IDKey(String var0) {
        if (md5 == null) {
            return var0;
        } else {
            md5.update(var0.getBytes());
            byte[] var1 = md5.digest();
            char[] var2 = new char[32];
            int var3 = 0;

            for (int var4 = 0; var4 < 16; ++var4) {
                byte var5 = var1[var4];
                var2[var3++] = hexDigits[var5 >>> 4 & 15];
                var2[var3++] = hexDigits[var5 & 15];
            }

            return new String(var2);
        }
    }

    public static String secondToShowString(long var0) {
        return null;
    }

    public static String fluxToShowString(int var0) {
        return null;
    }

    public static void main(String[] var0) {
        System.out.println(getMD5IDKey("0"));
        System.out.println(getMD5IDKey("1"));
    }

    public static String toUIStr(int var0) {
        return var0 == -2147483648 ? "" : "" + var0;
    }

    public static int uiStrToInt(String var0) {
        if (var0 != null && !var0.equals("")) {
            int var1 = Integer.parseInt(var0);
            return var1;
        } else {
            return -2147483648;
        }
    }

    public static void initObjectValue(Object var0) {
        try {
            Class var1 = var0.getClass();
            Field[] var2 = var1.getDeclaredFields();
            Random var3 = new Random(24234243L);

            for (int var4 = 0; var4 < var2.length; ++var4) {
                Field var5 = var2[var4];
                Class var6 = var5.getType();
                String var7 = var6.getName();
                String var8 = var5.getName();
                Class[] var9 = new Class[]{var6};
                String var10 = var8.replaceFirst(var8.substring(0, 1), var8.substring(0, 1).toUpperCase());
                Method var11 = var1.getMethod("set" + var10, var9);
                Object[] var12;
                if (var7.equalsIgnoreCase("int")) {
                    var12 = new Object[]{new Integer(var3.nextInt())};
                    var11.invoke(var0, var12);
                } else if (var7.equalsIgnoreCase("java.lang.String")) {
                    var12 = new Object[]{var5.getName()};
                    var11.invoke(var0, var12);
                } else if (var7.equalsIgnoreCase("java.sql.Timestamp")) {
                    var12 = new Object[]{new Timestamp(System.currentTimeMillis())};
                    var11.invoke(var0, var12);
                } else if (var7.equalsIgnoreCase("java.sql.Date")) {
                    var12 = new Object[]{new java.sql.Date(System.currentTimeMillis())};
                    var11.invoke(var0, var12);
                } else if (var7.equalsIgnoreCase("long")) {
                    var12 = new Object[]{new Long(var3.nextLong())};
                    var11.invoke(var0, var12);
                } else if (var7.equalsIgnoreCase("boolean")) {
                    var12 = new Object[]{Boolean.TRUE};
                    var11.invoke(var0, var12);
                }
            }
        } catch (Exception var13) {
            var13.printStackTrace();
        }

    }

    public static void formatObjectString(Object var0) {
        try {
            if (var0 == null) {
                return;
            }

            Class var1 = var0.getClass();
            Field[] var2 = var1.getDeclaredFields();

            for (int var3 = 0; var3 < var2.length; ++var3) {
                Field var4 = var2[var3];
                Class var5 = var4.getType();
                String var6 = var5.getName();
                String var7 = var4.getName();
                Class[] var8 = new Class[]{var5};
                if (var6.equalsIgnoreCase("java.lang.String")) {
                    String var9 = var7.replaceFirst(var7.substring(0, 1), var7.substring(0, 1).toUpperCase());
                    Class[] var10 = new Class[0];
                    Method var11 = var1.getMethod("get" + var9, var10);
                    Object[] var12 = new Object[0];
                    Object var13 = var11.invoke(var0, var12);
                    if (var13 == null) {
                        String var14 = var7.replaceFirst(var7.substring(0, 1), var7.substring(0, 1).toUpperCase());
                        Method var15 = var1.getMethod("set" + var14, var8);
                        Object[] var16 = new Object[]{""};
                        var15.invoke(var0, var16);
                    }
                }
            }
        } catch (Exception var17) {
            MyLog.err(res.getString("String_61") + var0 + res.getString("String_62") + var17.toString());
            MyLog.debug(var17);
        }

    }

    public static void formatObjectString(List var0) {
        for (int var1 = 0; var1 < var0.size(); ++var1) {
            formatObjectString(var0.get(var1));
        }

    }

    public static void formatObjectString(Object[] var0) {
        for (int var1 = 0; var1 < var0.length; ++var1) {
            formatObjectString(var0[var1]);
        }

    }
}


