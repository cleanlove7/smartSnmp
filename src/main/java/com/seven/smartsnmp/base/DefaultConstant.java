package com.seven.smartsnmp.base;

public class DefaultConstant {

	/**
	 * SNMP读取MIB时的索引定义
	 */
	//无索引
	public static final int MIB_OID_NO_INDEX = 0;
	//单索引
	public static final int MIB_OID_SINGLE_INDEX = 1;
	//双索引
	public static final int MIB_OID_DOUBLE_INDEX = 2;
	
	/**
	 * SNMP读写共同体类型
	 */
	//读SNMP
	public static final int SNMP_COMMUNITY_GET = 0;
	//写SNMP
	public static final int SNMP_COMMUNITY_SET = 1;
}
