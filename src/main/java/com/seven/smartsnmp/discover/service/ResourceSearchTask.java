package com.seven.smartsnmp.discover.service;

import com.seven.smartsnmp.discover.DeviceInfo;
import com.seven.smartsnmp.discover.ResourceInfo;
import com.seven.smartsnmp.discover.ResourceSearchMothType;

import java.util.Iterator;
import java.util.List;

public class ResourceSearchTask implements Runnable {

    DeviceInfo deviceInfo;
    List<ResourceInfo> rsList;
    ResourceSearchMothType resourceType;

    public ResourceSearchTask(DeviceInfo var1, ResourceSearchMothType var2, List<ResourceInfo> var3) {
        this.deviceInfo = var1;
        this.resourceType = var2;
        this.rsList = var3;
    }
    @Override
    public void run() {
        List var1 = this.resourceType.getResoureInfo(this.deviceInfo);
        Iterator var3 = var1.iterator();

        while (var3.hasNext()) {
            ResourceInfo var2 = (ResourceInfo) var3.next();
            if (var2 != null) {
                this.rsList.add(var2);
            }
        }

    }
}
