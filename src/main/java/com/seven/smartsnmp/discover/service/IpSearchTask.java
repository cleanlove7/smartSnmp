package com.seven.smartsnmp.discover.service;

import com.seven.smartsnmp.discover.DeviceInfo;
import com.seven.smartsnmp.discover.DeviceTypeInfo;
import com.seven.smartsnmp.snmp.SNMPTarget;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class IpSearchTask implements Runnable {
    String ip;
    List<SNMPTarget> snmpTargetList;
    ConcurrentLinkedQueue<DeviceInfo> retQueue;
    boolean isUsePing;

    public IpSearchTask(String ip, List<SNMPTarget> snmpTargetList, ConcurrentLinkedQueue<DeviceInfo> retQueue , boolean isUsePing) {
        this.snmpTargetList = snmpTargetList;
        this.ip = ip;
        this.retQueue = retQueue ;
        this.isUsePing = isUsePing;
    }
    @Override
    public void run() {
        try {
            DeviceInfo deviceInfo = (new SearchBasicDeviceInfoService()).getDeviceBasicInfo(this.ip, this.snmpTargetList, this.isUsePing);
            if (deviceInfo != null) {
                DeviceTypeInfo deviceType = (new SearchTypeService()).matchDeviceType(deviceInfo);
                deviceInfo.setDeviceType(deviceType);
                synchronized(this.retQueue) {
                    if (!this.retQueue.contains(deviceInfo)) {
                        this.retQueue.add(deviceInfo);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
