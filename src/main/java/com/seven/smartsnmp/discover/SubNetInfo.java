package com.seven.smartsnmp.discover;

import java.io.Serializable;

/**
 * @author lvbin
 */
public class SubNetInfo implements Serializable {
    private String netInfo = "";
    private String netMask = "";
    private String netRouterIP = "";

    public SubNetInfo() {
    }

    public void setNetInfo(String var1) {
        this.netInfo = var1;
    }

    public String getNetInfo() {
        return this.netInfo;
    }

    public void setNetRouterIP(String var1) {
        this.netRouterIP = var1;
    }

    public String getNetRouterIP() {
        return this.netRouterIP;
    }
}