package com.seven.smartsnmp.discover;


import java.io.Serializable;

/**
 * @author seven
 * @create 2019-06-17 13:36
 **/
public class LinkInfo implements Serializable {
    public DeviceInfo startDevice; //起始设备
    public DeviceInfo endDevice;   //结束设备
    public boolean isTrunk = false;
    public PortInfo startPortInfo;//起始都端口
    public PortInfo endPortInfo;
    public int startPort = -1;//起始端口
    public int endPort = -1; //结束端口
    public long linkBandwidth = -1L;
    public int linkLineType = -1;
    //连接类型
    public String linkType;
    public String linkDesc;
    public String linkParam;
    public String linkDispayMsg;
    public String linkFindProtocol;
    public static final int UnknownPort = -1;
    public static final int LinkLineTypeEthernet = 1;
    public static final int LinkLineTypeWifi = 2;
    public static final int LinkLineTypeOptical = 3;
    public static final int LinkLineTypeTelecom = 4;
    public static final String LinkTypeInbound = "内部链接";
    public static final String LinkTypeOutbound = "外部链接";

    public LinkInfo() {
        this.linkType = LinkTypeInbound;
        this.linkDesc = "";
        this.linkParam = "";
        this.linkDispayMsg = "";
        this.linkFindProtocol = "";
    }


    @Override
    public String toString() {
        return this.linkType + "," + this.startDevice + ",端口:" + (this.startPort == -1 ? "未知" : this.startPort) + ",MAC:" + this.startDevice.getDeviceMAC() + " <------> " + this.endDevice + ",端口：" + (this.endPort == -1 ? ",未知" : this.endPort) + ",MAC:" + this.endDevice.getDeviceMAC();

    }

    @Override
    public boolean equals(Object var1) {
        if (var1 instanceof LinkInfo) {
            LinkInfo var2 = (LinkInfo) var1;
            if (this.startDevice.equals(var2.startDevice) && this.startPort == var2.startPort && this.endDevice.equals(var2.endDevice) && this.endPort == var2.endPort) {
                return true;
            }
            if (this.startDevice.equals(var2.endDevice) && this.startPort == var2.endPort && this.endDevice.equals(var2.startDevice) && this.endPort == var2.startPort) {
                return true;
            }
        }

        return false;
    }

    /**
     * 修正连接关系
     *
     * @param linkInfo
     * @return
     */
    public int checkFuzzyOrAccurate(LinkInfo linkInfo) {
        if (this.equals(linkInfo)) {
            return 0;
        } else if (linkInfo.startDevice.equals(this.startDevice) && linkInfo.endDevice.equals(this.endDevice)) {
            int var2 = linkInfo.startPort;
            int var3 = linkInfo.endPort;
            if (-1 == this.startPort && -1 == this.endPort) {
                return 1;
            } else if (-1 == this.startPort && -1 != var2 && var3 == this.endPort) {
                return 1;
            } else if (-1 == this.endPort && -1 != var3 && var2 == this.startPort) {
                return 1;
            } else if (-1 == var2 && -1 == var3) {
                return 0;
            } else {
                if (-1 != this.startPort && -1 != this.endPort) {
                    if (this.startPort == var2 && -1 == var3) {
                        return 0;
                    }

                    if (this.endPort == var3 && -1 == var2) {
                        return 0;
                    }
                }

                if (this.startPort == -1 && var3 == -1) {
                    linkInfo.endPort = this.endPort;
                    linkInfo.endPortInfo = this.endPortInfo;

                    return 1;
                } else if (this.endPort == -1 && var2 == -1) {
                    linkInfo.startPort = this.startPort;
                    linkInfo.startPortInfo = this.startPortInfo;

                    return 1;
                } else {
                    return -1;
                }
            }
        } else {
            return -1;
        }
    }
}

