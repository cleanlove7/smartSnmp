package com.seven.smartsnmp.discover;

import com.seven.smartsnmp.mib.*;
import com.seven.smartsnmp.snmp.SNMPTarget;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class DeviceInfo {

    private String deviceIP = "";
    private String deviceMAC = "";
    private String deviceName = "";
    private String deviceProtocol = "";
    private String deviceDesc = "";
    private String deviceSysOID = "";
    private SNMPTarget snmpTarget = null;
    private DeviceTypeInfo deviceType;
    private List<MibIPAddrEntry> ipaddressList;
    private List<MibIPRouterEntry> routerTableList;
    private List<MibIfEntry> ifTableList;
    private List<Dot1dTpFdbEntry> dTpFdbTableList;
    private List<Dot1dBasePortEntry> basePortList;
    private List<LldpRemEntry> lldpRemTableList;
    private List<LldpLocPortEntry> locPortTableList;
    private List<CdpCacheEntry> cdpCacheTableList;
    private List<OspfNbrEntry> ospfNbrTableList;
    private List<OspfStubAreaEntry> ospfStubAreaEntryList;
    private List<MibMacIP> mibMacIPList;
    private List<String> deviceMacList;
    private List<PortInfo> portInfoList;
    public static final String DeviceProtocolSNMP = "SNMP";
    public static final String DeviceProtocolPing = "Ping";
    public static final String DeviceProtocolTelnet = "Telnet";
    public static final String DeviceProtocolSSH = "SSH";
    public static final String DeviceProtocolWMI = "WMI";
    public static final String DeviceProtocolNULL = "";

    public DeviceInfo() {
        this.deviceType = DeviceTypeInfo.UnkownType;
        this.ipaddressList = new CopyOnWriteArrayList();
        this.routerTableList = new CopyOnWriteArrayList();
        this.ifTableList = new CopyOnWriteArrayList();
        this.dTpFdbTableList = new CopyOnWriteArrayList();
        this.basePortList = new CopyOnWriteArrayList();
        this.mibMacIPList = new CopyOnWriteArrayList();
        this.deviceMacList = new CopyOnWriteArrayList();
        this.portInfoList = new CopyOnWriteArrayList<>();
    }

    @Override
    public String toString() {
        return "IP:" + this.deviceIP + ",name:" + this.deviceName + ",type:" + this.deviceType;
    }

    public String getDeviceIDCode() {
        String var1 = "";

        MibIfEntry var2;
        for (Iterator var3 = this.ifTableList.iterator(); var3.hasNext(); var1 = var1 + var2.getIfPhysAddress()) {
            var2 = (MibIfEntry) var3.next();
        }

        return var1;
    }

    public List<LldpRemEntry> getLldpRemTableList() {
        return lldpRemTableList;
    }

    public void setLldpRemTableList(List<LldpRemEntry> lldpRemTableList) {
        this.lldpRemTableList = lldpRemTableList;
    }

    public List<LldpLocPortEntry> getLocPortTableList() {
        return locPortTableList;
    }

    public void setLocPortTableList(List<LldpLocPortEntry> locPortTableList) {
        this.locPortTableList = locPortTableList;
    }

    public List<CdpCacheEntry> getCdpCacheTableList() {
        return cdpCacheTableList;
    }

    public void setCdpCacheTableList(List<CdpCacheEntry> cdpCacheTableList) {
        this.cdpCacheTableList = cdpCacheTableList;
    }

    public List<OspfNbrEntry> getOspfNbrTableList() {
        return ospfNbrTableList;
    }

    public void setOspfNbrTableList(List<OspfNbrEntry> ospfNbrTableList) {
        this.ospfNbrTableList = ospfNbrTableList;
    }

    @Override
    public boolean equals(Object var1) {
        if (var1 instanceof DeviceInfo) {
            DeviceInfo var2 = (DeviceInfo) var1;
            if (this.getDeviceIP().equals(var2.getDeviceIP())) {
                return true;
            }

            if (var2.getDeviceMAC() != null && !var2.getDeviceMAC().equals("") && !var2.getDeviceMAC().equals("00:00:00:00:00:00") && var2.getDeviceMAC().equalsIgnoreCase(this.getDeviceMAC())) {
                return true;
            }

            if (!this.getDeviceIDCode().equals("") && this.getDeviceIDCode().equals(var2.getDeviceIDCode())) {
                System.out.println("重复的设备:" + this + ";" + var1);
                return true;
            }
        }
        return false;
    }

    public void setDeviceSysOID(String var1) {
        this.deviceSysOID = var1;
    }

    public String getDeviceSysOID() {
        return this.deviceSysOID;
    }

    public String getDeviceIP() {
        return this.deviceIP;
    }

    public void setDeviceIP(String var1) {
        this.deviceIP = var1;
    }

    public List<String> getDeviceMacList() {
        return this.deviceMacList;
    }

    public void setDeviceMacList(List<String> var1) {
        this.deviceMacList = var1;
    }

    public String getDeviceMAC() {
        return this.deviceMAC;
    }

    public void setDeviceMAC(String var1) {
        this.deviceMAC = var1;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String var1) {
        this.deviceName = var1;
    }

    public String getDeviceDesc() {
        return this.deviceDesc;
    }

    public void setDeviceDesc(String var1) {
        this.deviceDesc = var1;
    }

    public SNMPTarget getSnmpTarget() {
        return this.snmpTarget;
    }

    public void setSnmpTarget(SNMPTarget var1) {
        this.snmpTarget = var1;
    }

    public void setDeviceType(DeviceTypeInfo var1) {
        this.deviceType = var1;
    }

    public DeviceTypeInfo getDeviceType() {
        return this.deviceType;
    }

    public void setDeviceProtocol(String var1) {
        this.deviceProtocol = var1;
    }

    public String getDeviceProtocol() {
        return this.deviceProtocol;
    }

    public void setIfTableList(List<MibIfEntry> var1) {
        this.ifTableList = var1;
    }

    public List<MibIfEntry> getIfTableList() {
        return this.ifTableList;
    }

    public List<MibMacIP> getMibMacIPList() {
        return this.mibMacIPList;
    }

    public void setMibMacIPList(List<MibMacIP> var1) {
        this.mibMacIPList = var1;
    }

    public List<PortInfo> getPortInfoList() {
        return this.portInfoList;
    }

    public void setPortInfoList(List<PortInfo> var1) {
        this.portInfoList = var1;
    }

    public List<Dot1dTpFdbEntry> getdTpFdbTableList() {
        return this.dTpFdbTableList;
    }

    public void setdTpFdbTableList(List<Dot1dTpFdbEntry> var1) {
        this.dTpFdbTableList = var1;
    }

    public List<MibIPAddrEntry> getIpaddressList() {
        return this.ipaddressList;
    }

    public void setIpaddressList(List<MibIPAddrEntry> var1) {
        this.ipaddressList = var1;
    }

    public List<MibIPRouterEntry> getRouterTableList() {
        return this.routerTableList;
    }

    public void setRouterTableList(List<MibIPRouterEntry> var1) {
        this.routerTableList = var1;
    }

    public List<Dot1dBasePortEntry> getBasePortList() {
        return this.basePortList;
    }

    public void setBasePortList(List<Dot1dBasePortEntry> var1) {
        this.basePortList = var1;
    }

    public List<OspfStubAreaEntry> getOspfStubAreaEntryList() {
        return ospfStubAreaEntryList;
    }

    public void setOspfStubAreaEntryList(List<OspfStubAreaEntry> ospfStubAreaEntryList) {
        this.ospfStubAreaEntryList = ospfStubAreaEntryList;
    }
}
