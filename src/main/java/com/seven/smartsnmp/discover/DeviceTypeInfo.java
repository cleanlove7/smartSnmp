package com.seven.smartsnmp.discover;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author seven
 * @create 2019-06-17 10:14
 **/
public class DeviceTypeInfo implements Serializable {
    private String deviceProductType = "";
    private String logicType = "";
    private List<String> typeOIDCharacter = new CopyOnWriteArrayList();
    private List<String> typeDescCharacter = new CopyOnWriteArrayList();

    public static final DeviceTypeInfo UnkownType = new DeviceTypeInfo();
    public static final DeviceTypeInfo SwitchType = new DeviceTypeInfo();
    public static final DeviceTypeInfo RouterType = new DeviceTypeInfo();
    public static final DeviceTypeInfo RouterSwitchType = new DeviceTypeInfo();
    public static final DeviceTypeInfo ComputerType = new DeviceTypeInfo();

    static {
       UnkownType.setDeviceProductType("其他设备类型");
       UnkownType.setLogicType("其他设备类型");
       SwitchType.setDeviceProductType("交换机");
       SwitchType.setLogicType("交换机");
       RouterType.setDeviceProductType("路由器");
       RouterType.setLogicType("路由器");
       RouterSwitchType.setDeviceProductType("路由交换机");
       RouterSwitchType.setLogicType("路由交换机");
       ComputerType.setDeviceProductType("计算机");
       ComputerType.setLogicType("计算机");
    }

    public DeviceTypeInfo() {
    }

    @Override
    public String toString() {
        return this.deviceProductType;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DeviceTypeInfo) {
            DeviceTypeInfo var2 = (DeviceTypeInfo) obj;
            if (this.getDeviceProductType().equals(var2.getDeviceProductType())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断物理类型是否相同
     * @param obj
     * @return
     */
    public boolean equalsLogicType(Object obj) {
        if (obj instanceof DeviceTypeInfo) {
            DeviceTypeInfo deviceTypeInfo = (DeviceTypeInfo) obj;
            if (this.getLogicType().equalsIgnoreCase(deviceTypeInfo.getLogicType())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.deviceProductType.hashCode();
    }

    public void setDeviceProductType(String var1) {
        this.deviceProductType = var1;
    }

    public String getDeviceProductType() {
        return this.deviceProductType;
    }

    public void setLogicType(String var1) {
        this.logicType = var1;
    }

    public String getLogicType() {
        return this.logicType;
    }

    public void setTypeOIDCharacter(List<String> var1) {
        this.typeOIDCharacter = var1;
    }

    public List<String> getTypeOIDCharacter() {
        return this.typeOIDCharacter;
    }

    public void setTypeDescCharacter(List<String> var1) {
        this.typeDescCharacter = var1;
    }

    public List<String> getTypeDescCharacter() {
        return this.typeDescCharacter;
    }
}