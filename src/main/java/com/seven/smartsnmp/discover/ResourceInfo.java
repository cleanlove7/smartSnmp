package com.seven.smartsnmp.discover;

import java.io.Serializable;

public class ResourceInfo implements Serializable {
    private DeviceInfo localDevice = null;
    private String resourceTypeName = "";
    private String resourceRSIDValue = "";
    private String resourceName = "";
    private String resourceParam = "";
    private String resourceDesc = "";
    private String resrouceRemarks = "";
    private Object resourceObjectInfo;

    public ResourceInfo() {
    }

    public boolean equals(ResourceInfo var1) {
        return var1 instanceof ResourceInfo && this.localDevice.equals(var1.getLocalDevice()) && this.resourceTypeName.equals(var1.getResourceTypeName()) && this.resourceRSIDValue.equals(var1.getResourceRSIDValue());
    }

    public DeviceInfo getLocalDevice() {
        return this.localDevice;
    }

    public void setLocalDevice(DeviceInfo var1) {
        this.localDevice = var1;
    }

    public String getResourceTypeName() {
        return this.resourceTypeName;
    }

    public void setResourceTypeName(String var1) {
        this.resourceTypeName = var1;
    }

    public String getResourceDesc() {
        return this.resourceDesc;
    }

    public void setResourceDesc(String var1) {
        this.resourceDesc = var1;
    }

    public String getResourceParam() {
        return this.resourceParam;
    }

    public void setResourceParam(String var1) {
        this.resourceParam = var1;
    }

    public void setResourceRSIDValue(String var1) {
        this.resourceRSIDValue = var1;
    }

    public String getResourceRSIDValue() {
        return this.resourceRSIDValue;
    }

    public void setResourceName(String var1) {
        this.resourceName = var1;
    }

    public String getResourceName() {
        return this.resourceName;
    }

    public void setResourceObjectInfo(Object var1) {
        this.resourceObjectInfo = var1;
    }

    public Object getResourceObjectInfo() {
        return this.resourceObjectInfo;
    }

    public String getResrouceRemarks() {
        return this.resrouceRemarks;
    }

    public void setResrouceRemarks(String var1) {
        this.resrouceRemarks = var1;
    }
}
