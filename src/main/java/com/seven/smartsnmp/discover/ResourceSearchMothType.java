package com.seven.smartsnmp.discover;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract  class ResourceSearchMothType {

    public String resoureTypeName = "";
    public String resouceBaseType = "";
    public List<String> typeSoftDescList = new CopyOnWriteArrayList<>();
    public String resourceRSID = "";
    public String typeDesc = "";
    public String displayNameCode = "";
    public String paramCode = "";
    public static final String ResouceBaseTypeSNMPTable = "路由器";
    public static final String ResouceBaseTypeSNMPGroup = "路由器";
    public static final String ResouceBaseTypeTCPServer = "路由交换机";
    public static final String ResouceBaseTypeSystem = "路由交换机";
    public ResourceSearchMothType() {
    }

    public abstract List<ResourceInfo> getResoureInfo(DeviceInfo var1);

    public abstract String getResourceSearchMoth();

    public boolean equals(Object var1) {
        if (var1 instanceof ResourceSearchMothType) {
            ResourceSearchMothType var2 = (ResourceSearchMothType)var1;
            if (this.getResoureTypeName().equals(var2.getResoureTypeName())) {
                return true;
            }
        }

        return false;
    }

    public int hashCode() {
        return this.resoureTypeName.hashCode();
    }

    public String getResoureTypeName() {
        return this.resoureTypeName;
    }

    public void setResoureTypeName(String var1) {
        this.resoureTypeName = var1;
    }

    public String getResourceRSID() {
        return this.resourceRSID;
    }

    public void setResourceRSID(String var1) {
        this.resourceRSID = var1;
    }

    public String getResouceBaseType() {
        return this.resouceBaseType;
    }

    public void setResouceBaseType(String var1) {
        this.resouceBaseType = var1;
    }

    public String getTypeDesc() {
        return this.typeDesc;
    }

    public void setTypeDesc(String var1) {
        this.typeDesc = var1;
    }

    public List<String> getTypeSoftDescList() {
        return this.typeSoftDescList;
    }

    public void setTypeSoftDescList(List<String> var1) {
        this.typeSoftDescList = var1;
    }

    public String getDisplayNameCode() {
        return this.displayNameCode;
    }

    public void setDisplayNameCode(String var1) {
        this.displayNameCode = var1;
    }

    public String getParamCode() {
        return this.paramCode;
    }

    public void setParamCode(String var1) {
        this.paramCode = var1;
    }

    public static String getResoucebasetypesnmptable() {
        return ResouceBaseTypeSNMPTable;
    }

    public static String getResoucebasetypesnmpgroup() {
        return ResouceBaseTypeSNMPGroup;
    }

    public static String getResoucebasetypetcpserver() {
        return ResouceBaseTypeTCPServer;
    }

    public static String getResoucebasetypesystem() {
        return ResouceBaseTypeSystem;
    }
}
